# Python ETL Example

This is an example ETL pipeline written in Python.

# Architecture

```
 +-------------+                          +--------------+              +----------------------------+
 |  source ms  <-------------------------->              |              | json                       |
 |             |        GET / every       |   producer   +-----+        | {                          |
 +-------------+          30s             |      ms      |     |        |  "name": "random",         |
                                          +--------------+     |        |  "latitude": "75.12345",   |
                                                               |        |  "longitude": "123.23456"  |
  +------------------------------------------------------+     |        | }                          |
  |                                                      |     |        +____________________________+
  |                     kafka queue                      <-----+                       source payload
  |                                                      |
  +-----------^------------------------------------------+
              |
              |   +-------------------+        +-----------------+
              |   |    consumer ms    |        |                 |
              ^---+                   +-------->                 |
              |   +-------------------+        |                 |      +----------------------------+
              |   +-------------------+        |                 |      | json                       |
              |   |    consumer ms    +-------->                 |      | {                          |
              +---+                   |        |  elasticsearch  |      |  "name": "random",         |
                  +-------------------+        |                 |      |  "latitude": "75.12345",   |
                          ...                  |                 |      |  "longitude": "123.23456", |
                                               |                 |      |  "timestamp": "20210923"   |
                                               |                 |      | }                          |
                                               |                 |      +----------------------------+
                                               +-----------------+                transformed payload
```

- The Producer microservice fetches data from the random data generator (Source microservice) and publishes it to the Kafka queue every 30 seconds.
- The Consumer microservice(s) will automatically consume messages from the Kafka queue, modify the data (by adding a timestamp), and write the data to Elasticsearch.

## Requirements

- Docker/Docker-compose

## Environment Variables

All environment variables are included in the .env file and will be read by docker-compose.

You may overwrite these defaults by modifying the .env file or by exporting in the terminal.

## Usage

1. Start all microservices
```bash
docker-compose up
```

- Note: the first time `docker-compose up` is run, it will automatically build the flask microservices. To force a rebuild, use: `docker-compose up --build`
