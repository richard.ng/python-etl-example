# Source Microservice

This is an example data source microservice written in Python.

## Background

This microservice is a Flask app that will randomly generate and return a JSON payload when sending a GET request to the / endpoint.

```json
{
  "name": "abcdefghij",
  "latitude": 23.21382,
  "longitude": 134.67923
}
```

## Building

This microservice is written in a way that should allow a different source of randomly generated data to be used without much refactoring.

Run `docker build -t source:latest .` to build a docker image with the lastest changes.

## Testing

Run `pytest` to run all tests in the ./app/tests/ directory.
