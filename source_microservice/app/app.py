import os
from flask import Flask
from city_generator import CityGenerator
from random_source import RandomSource

SOURCE_PORT = os.getenv("SOURCE_PORT")

app = Flask(__name__)


@app.route("/healthz")
def healthz():
    return {"status": "healthy"}


@app.route("/")
def generate_city():
    cg = CityGenerator(RandomSource())
    return cg.generate()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=SOURCE_PORT)
