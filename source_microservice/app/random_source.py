import string
import random

class RandomSource:
    def random_string(self):
        letters = string.ascii_lowercase
        result = ''.join(random.choice(letters) for i in range(10))
        return result

    def generate(self):
        name = self.random_string()
        latitude = round(random.uniform(-90.0, 90.0), 5)
        longitude = round(random.uniform(-180.0, 180.0), 5)
        return {
            "name": name,
            "latitude": latitude,
            "longitude": longitude
            }
