from pytest_mock import mocker
from app.city_generator import CityGenerator


def test_generate(mocker):
    class FakeSource:
        def generate(self):
            return {}

    fs = FakeSource()
    city = CityGenerator(fs)

    spy = mocker.spy(fs, "generate")

    assert city.generate() == {}

    assert spy.call_count == 1
