import pytest
from app.random_source import RandomSource

def test_random_string():
    source = RandomSource()

    actual = source.random_string()

    assert isinstance(actual, str) == True
    assert len(actual) == 10

def test_generate():
    source = RandomSource()

    actual = source.generate()

    assert isinstance(actual, dict) == True
    assert "name" in actual
    assert "latitude" in actual
    assert "longitude" in actual
    assert isinstance(actual["latitude"], float) == True
    assert isinstance(actual["longitude"], float) == True
