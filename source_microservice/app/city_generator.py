class CityGenerator:
    def __init__(self, city_source):
        self.city_source = city_source

    def generate(self):
        return self.city_source.generate()
