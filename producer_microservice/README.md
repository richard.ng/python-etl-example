# Producer Microservice

This is an example Kafka producer microservice written in Python.

## Background

This microservice is a Flask app that will poll the Source microservice at 30 second intervals and publish the returned JSON payload to a Kafka queue.

## Building

This microservice is written in a way that should allow a different source of data to be used without much refactoring.

Run `docker build -t producer:latest .` to build a docker image with the lastest changes.

## Testing

Run `pytest` to run all tests in the ./app/tests/ directory.
