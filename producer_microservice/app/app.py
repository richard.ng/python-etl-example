import os
import threading
import time
from json import dumps
from kafka import KafkaProducer, errors
from flask import Flask

from data_source import DataSource
from custom_source import CustomSource

PRODUCER_PORT = os.getenv("PRODUCER_PORT")
KAFKA_SERVER = os.getenv("KAFKA_SERVER")
KAFKA_TOPIC = os.getenv("KAFKA_TOPIC")
SOURCE_URL = os.getenv("SOURCE_URL")

app = Flask(__name__)


@app.route("/healthz")
def healthz():
    return {"status": "healthy"}


while True:
    try:
        producer = KafkaProducer(
                bootstrap_servers=[KAFKA_SERVER],
                value_serializer=lambda x: dumps(x).encode("utf-8")
                )
        break
    except errors.NoBrokersAvailable:
        print("No Brokers available ... retrying")
        time.sleep(30)


def publish_data():
    while True:
        ds = DataSource(CustomSource(SOURCE_URL))
        data = ds.get().json()
        producer.send(KAFKA_TOPIC, value=data)
        print(data)
        time.sleep(30)


if __name__ == "__main__":
    t = threading.Thread(target=publish_data)
    t.start()
    app.run(host="0.0.0.0", port=PRODUCER_PORT)
