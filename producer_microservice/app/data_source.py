class DataSource:
    def __init__(self, data):
        self.data = data

    def get(self):
        return self.data.get()
