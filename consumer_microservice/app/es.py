from elasticsearch import Elasticsearch

es = Elasticsearch(["elasticsearch:9200"])

class ElasticSearch:
    def __init__(self, index):
        self.index = index

    def count(self):
        return es.count(index=self.index)["count"]

    def create_record(self, id, body):
        return es.index(index=self.index, id=id, body=body)

    def exists(self):
        return es.indices.exists(index=self.index)

    def create_index(self):
        return es.indices.create(self.index, ignore=400)

    def refresh(self):
        return es.indices.refresh(self.index)
