class DataStore:
    def __init__(self, data_store):
        self.data_store = data_store

    def count(self):
        return self.data_store.count()

    def create_record(self, *args):
        return self.data_store.create_record(*args)

    def exists(self):
        return self.data_store.exists()

    def create_index(self):
        return self.data_store.create_index()

    def refresh(self):
        return self.data_store.refresh()
