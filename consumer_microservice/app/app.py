import os
import threading
import time
from datetime import datetime
from json import loads
from kafka import KafkaConsumer, errors
from flask import Flask

from data_store import DataStore
from es import ElasticSearch

CONSUMER_PORT = os.getenv("CONSUMER_PORT")
KAFKA_SERVER = os.getenv("KAFKA_SERVER")
KAFKA_TOPIC = os.getenv("KAFKA_TOPIC")
KAFKA_GROUP_ID = os.getenv("KAFKA_GROUP_ID")
ELASTICSEARCH_INDEX = os.getenv("ELASTICSEARCH_INDEX")

app = Flask(__name__)


@app.route("/healthz")
def healthz():
    return {"status": "healthy"}


while True:
    try:
        consumer = KafkaConsumer(
                KAFKA_TOPIC,
                bootstrap_servers=[KAFKA_SERVER],
                auto_offset_reset="earliest",
                enable_auto_commit=True,
                group_id=KAFKA_GROUP_ID,
                value_deserializer=lambda x: loads(x.decode("utf-8"))
                )
        break
    except errors.NoBrokersAvailable:
        print("No Brokers available ... retrying")
        time.sleep(30)


def consume_data():
    ds = DataStore(ElasticSearch(ELASTICSEARCH_INDEX))
    if not ds.exists():
        ds.create_index()

    for event in consumer:
        count = ds.count()
        id = count + 1

        event.value["timestamp"] = datetime.now()

        ds.create_record(id, event.value)
        ds.refresh()
        print(id, event.value)


if __name__ == "__main__":
    t = threading.Thread(target=consume_data)
    t.start()
    app.run(host="0.0.0.0", port=CONSUMER_PORT)
