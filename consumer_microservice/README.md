# Consumer Microservice

This is an example Kafka consumer microservice written in Python.

## Background

This microservice is a Flask app that will automatically read messages from the Kafka queue and modify them by adding a timestamp before writing the data to Elasticsearch.

```json
{
  "name": "abcdefghij",
  "latitude": 23.21382,
  "longitude": 134.67923,
  "timestamp": "2021-07-04T07:25:02.623785"
}
```

## Building

This microservice is written in a way that should allow a different data store to be used without much refactoring.

Run `docker build -t consumer:latest .` to build a docker image with the lastest changes.

## Testing

Run `pytest` to run all tests in the ./app/tests/ directory.
